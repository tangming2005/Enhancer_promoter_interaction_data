
### How to use the data

The data were downloaded from this paper:  
[Reconstruction of enhancer–target networks in 935 samples of human primary cells, tissues and cell lines](https://www.nature.com/articles/ng.3950)

Download from http://yiplab.cse.cuhk.edu.hk/jeme/

See how I processed the data [here](https://gitlab.com/tangming2005/Enhancer_promoter_interaction_data/blob/master/bedpe/README.md): 

1. Inside the `bed` folder: those are bed12 files you can upload to IGV or UCSC to visualize the interaction.
2. Inside the `bedpe` folder: those are bedpe files afer merging 127 ENCODE data and 800 FANTOM5 data.
3. If you need to assign your own enhancer data with a promoter, use the `ENCODE_FANTOM5_EP_refseq_promoter.tsv` inside the `annotation` folder and follow the instruction below.


### asign your own enhancer data to the refseq promoter

Now, you have your own H3K27ac ChIP-seq as potential enhancers, first exclude peaks around TSS (2kb around). e.g. `my_H3K27ac_exclude_promoter.bed`. (it it should be a 4-column file: chr, start, end, info). The last column should contain some information e.g. cluster id or dummy names.

you can now cut out the first 3 columns of the `ENCODE_FANTOM5_EP_refseq_promoter.tsv` file.

```bash
cut -f 1-3 ENCODE_FANTOM5_EP_refseq_promoter.tsv > potential_enhancer.bed

## check how many of your enhancers have overlapping
bedtools intersect -a my_H3K27ac_exclude_promoter.bed -b potential_enhancer.bed -wa | sort | uniq | wc -l

#
bedtools intersect -a potential_enhancer.bed -b my_H3K27ac_exclude_promoter.bed -wa -wb > overlaping.tsv

```

then, use R to do a left-join of the `overlapping.tsv` with `ENCODE_FANTOM5_EP_refseq_promoter.tsv` file to get the annotations.

```r
library(dplyr)
library(readr)

enhancer_file<- "~/projects/SKCM_project/overlapping.tsv"
  
enhancer<- read_tsv(enhancer_file, col_names = F)
names(enhancer)<- c("E1_chr", "E1_start", "E1_end", "E2_chr", "E2_start", "E2_end", "info")

annoation_file<- "~/projects/SKCM_project/ENCODE_FANTOM5_EP_refseq_promoter.tsv"
annotation<- read_tsv(annoation_file, col_names = F)

names(annotation)<- c("E1_chr", "E1_start", "E1_end", "P1_chr", "P1_start", "P1_end", "locus", "ref_transcript", "ref_gene", "strand", "P2_chr", "P2_start", "P2_end")

annotated_enhancer<- left_join(enhancer, annotation) %>% distinct(E2_chr, E2_start, E2_end, ref_gene, strand, .keep_all = T)

annotated_enhancer
# A tibble: 3,806 x 17
   E1_chr E1_start E1_end E2_chr E2_start E2_end              info P1_chr P1_start  P1_end locus ref_transcript ref_gene strand P2_chr P2_start  P2_end
    <chr>    <int>  <int>  <chr>    <int>  <int>             <chr>  <chr>    <int>   <int> <chr>          <chr>    <chr>  <chr>  <chr>    <int>   <int>
 1   chr1   870200 872600   chr1   870095 879582 enhancer_cluster3   chr1   858260  862260     B      NM_152486   SAMD11      +   chr1   859121  863121
 2   chr1   870200 872600   chr1   870095 879582 enhancer_cluster3   chr1   892689  897967     B      NM_015658    NOC2L      -   chr1   892679  896679
 3   chr1   870200 872600   chr1   870095 879582 enhancer_cluster3   chr1   892689  897967     B      NM_198317   KLHL17      +   chr1   893967  897967
 4   chr1   870200 872600   chr1   870095 879582 enhancer_cluster3   chr1   899877  903877     B   NM_001160184  PLEKHN1      +   chr1   899877  903877
 5   chr1   870200 872600   chr1   870095 879582 enhancer_cluster3   chr1   915497  919497     B   NM_001291366    PERM1      -   chr1   915497  919497
 6   chr1   870200 872600   chr1   870095 879582 enhancer_cluster3   chr1   933552  937552     B   NM_001142467     HES4      -   chr1   933552  937552
 7   chr1   870200 872600   chr1   870095 879582 enhancer_cluster3   chr1   946803  950803     B      NM_005101    ISG15      +   chr1   946847  950847
 8   chr1   870200 872600   chr1   870095 879582 enhancer_cluster3   chr1   953503  957503     B      NM_198576     AGRN      +   chr1   953503  957503
 9   chr1   870200 872600   chr1   870095 879582 enhancer_cluster3   chr1  1007687 1011687     B   NM_001205252   RNF223      -   chr1  1007687 1011687
10   chr1   870200 872600   chr1   870095 879582 enhancer_cluster3   chr1  1049741 1053741     B      NM_017891 C1orf159      -   chr1  1049736 1053736
```