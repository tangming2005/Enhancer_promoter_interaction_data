

# My note on how I get those files

### Enhancer promoter interaction public data sets

#### DnaseI-seq based:

[The accessible chromatin landscape of the human genome](https://www.nature.com/articles/nature11232)

>Genomic  coordinates  of  all  promoter  DHSs  and  distal,  non-promoter  DHSs  within ±500 kb correlated with them at threshold 0.7. Due to the size
of this file, we are making it available through the EBI ftp server at
ftp://ftp.ebi.ac.uk/pub/databases/ensembl/encode/integration_data_jan2011/byDataType/openchrom/jan2011/dhs_gene_connectivity/genomewideCorrs_above0.7_promoterPlusMinus500kb_withGeneNames_32celltypeCategories.bed8.gz

>This  compressed,  tab-delimited  text  file  contains  1,672,958 
lines  of  data,  for  63,318 distinct  promoter  DHSs that each have at least one distal DHS connected to it. Each promoter DHS overlaps a TSS, or is 
the  nearest  DHS  to  the  TSS  in  the  5’  direction;  columns  1-3  contain  each  promoter  DHS’s  genomic  coordinates (hg19). The Gencode gene names are given in column 4. Because distinct gene names can be  given  to  the  same  TSS,  and  because  distinct  TSSs  can  have  the  same  nearby  DHS  called  as  their promoter DHS, data for each promoter DHS is repeated in this file roughly three times on average, with a different gene name for each repetition (there are 207,878 distinct combinations of promoter DHS + gene name in this file). Columns 5-7 contain the genomic coordinates for each diqin 500kb  of  the  promoter  DHS  given  in  columns  1-3 that achieves correlation ≥0.7 with it; the correlation 
between the promoter/distal DHS pair is given in column 8. Distal DHSs appear multiple times in the file when they achieve correlation ≥0.7 with multiple promoter DHSs. Using program sort-bed  from  the  BEDOPS  genomic  data  analysis  software  suite,  from  the  command  line  within  a  Unix  system,  the  set  of  578,905 distal  DHSs  connected  with  at  least  one  promoter  DHS  can  be  extracted  into  a  file  named  “outfile” by executing the command

`cut –f5-7 infile | sort-bed - | uniq > outfile`

where “infile” represents the file
`genomewideCorrs_above0.7_promoterPlusMinus500kb_withGeneNames_32celltypeCategories.bed8`

```bash
cd /data/enhancer_promoter_interaction/

wget ftp://ftp.ebi.ac.uk/pub/databases/ensembl/encode/integration_data_jan2011/byDataType/openchrom/jan2011/dhs_gene_connectivity/genomewideCorrs_above0.7_promoterPlusMinus500kb_withGeneNames_32celltypeCategories.bed8.gz
```

**There are too many rows, I decided not to use this data set for now as many interactions may not be real**

#### histones and eRNA based 
[Reconstruction of enhancer–target networks in 935 samples of human primary cells, tissues and cell lines](https://www.nature.com/articles/ng.3950)

Download from http://yiplab.cse.cuhk.edu.hk/jeme/

>The first field is the location of the enhancer (0-based, left-close, right-open)
The second field is the list of transcripts sharing the TSS separated by %
The third field is the confidence score of the enhancer-target connection, with a higher score indicating a more confident prediction 

```bash
#only for encode and roadmap data
mkdir Encoderoadmap_EP_interaction
cd Encoderoadmap_EP_interaction

wget http://yiplab.cse.cuhk.edu.hk/jeme/encoderoadmap_lasso.zip

```


#### CAGE based from phantom5

download from http://enhancer.binf.ku.dk/presets/

```bash
# enhancer-RefSeq promoter associations 


wget http://enhancer.binf.ku.dk/presets/enhancer_tss_associations.bed

```

**Note this data set is somewhat overlapping with the lasso FANTOM5 data below, we will not use this data as well after I inspecting mannually for the VEGFA locus**

#### Tidy GAGE tss-enhancer bed12 file

```bash
less -S enhancer_tss_associations.bed  |  sed '1d' | body grep -w CD2 
#chrom  chromStart  chromEnd    name    score   strand  thickStart  thickEnd    itemRgb blockCount  blockSizes  chromStarts
chr1    117280568   117297586   chr1:117280613-117280924;NM_001767;CD2;R:0.345;FDR:0    345 .   117280768   117280769   0,0,0   2   401,1001    0,16017
chr1    117281371   117297586   chr1:117281390-117281752;NM_001767;CD2;R:0.353;FDR:0    353 .   117281571   117281572   0,0,0   2   401,1001    0,15214
chr1    117296585   117306342   chr1:117305811-117306472;NM_001767;CD2;R:0.496;FDR:0    496 .   117306141   117306142   0,0,0   2   1001,401    0,9356
chr1    117296585   117313658   chr1:117313335-117313579;NM_001767;CD2;R:0.186;FDR:1.04115776773157e-06 186 .   117313457   117313458   0,0,0   2   1001,401    0,16672
chr1    117296585   117347782   chr1:117347522-117347641;NM_001767;CD2;R:0.299;FDR:0    299 .   117347581   117347582   0,0,0   2   1001,401    0,50796
chr1    117296585   117359519   chr1:117359174-117359463;NM_001767;CD2;R:0.338;FDR:0    338 .   117359318   117359319   0,0,0   2   1001,401    0,62533


```

`chrom  chromStart  chromEnd` is for the whole block.

```bash

less -S enhancer_tss_associations.bed  |  sed '1,2d' | cut -f1-3,11 | tr "," "\t" |awk 'NF' |  awk -v OFS="\t" '{print $1,$2,$2+$4,$1,$3-$5,$3}' | pgltools formatbedpe > CAGE_enhancer_tss_associations.bedpe

```


DNaseI-seq based predictions:

```bash
less -S genomewideCorrs_above0.7_promoterPlusMinus500kb_withGeneNames_32celltypeCategories.bed8 | cut -f1-3,5-7 | pgltools formatbedpe > DHS_enhancer_promoter_associations.bedpe

```

### merge data sets for the ENCODE lasso predictions (127 files)

useful tool: [pgltools](https://github.com/billgreenwald/pgltools)  

For the ENCODE data, the first column is the enhancer, the second column is the TSS, I checked several entries mannually, and found some of them are several bases off from the refGene downloaded from UCSC. It is OK for now just 
expand the tss 2kb upstream and 2kb downstream.


```bash
cd /home/mtang1/projects/CCLE_project/data/enhancer_promoter_interaction/Encoderoadmap_EP_interaction/encoderoadmap_lasso
head encoderoadmap_lasso.1.csv 
chrX:100040000-100041800,ENSG00000000003.10$TSPAN6$chrX$99894988$-,0.49
chrX:100046800-100048000,ENSG00000000003.10$TSPAN6$chrX$99894988$-,0.37
chrX:100128800-100130000,ENSG00000000003.10$TSPAN6$chrX$99894988$-,0.39
chrX:99749000-99751000,ENSG00000000003.10$TSPAN6$chrX$99894988$-,0.47
chrX:99851000-99853000,ENSG00000000003.10$TSPAN6$chrX$99894988$-,0.57
chrX:99854000-99856200,ENSG00000000003.10$TSPAN6$chrX$99894988$-,0.68
chrX:99858800-99859600,ENSG00000000003.10$TSPAN6$chrX$99894988$-,0.59
chrX:99863600-99865600,ENSG00000000003.10$TSPAN6$chrX$99894988$-,0.57
chrX:99866800-99867800,ENSG00000000003.10$TSPAN6$chrX$99894988$-,0.55
chrX:99868400-99868600,ENSG00000000003.10$TSPAN6$chrX$99894988$-,0.55

### reformat to pgl format to use the pgltools
### $ sign is tricky to play with in grep, tr to : first
## extend 2kb on the tss 

cat   encoderoadmap_lasso.1.csv | tr "$" ":" |  sed -E 's/(chr[0-9XY]+:[0-9]+-[0-9]+),.+(chr[0-9XY]+:[0-9]+):.+/\1,\2/' | tr ":,-" "\t" | awk -v OFS="\t" '{print $1,$2,$3,$4,$5-2000,$5+2000}' | pgltools formatbedpe |  sed 's/[ \t]*$//'> encoderoadmap_lasso.bedpe

```

`csv2bedpe.sh`:

```bash
#! /bin/bash

set -eu
set -o pipefail

oprefix=$(basename $1 ".csv")

cat  $1 | tr "$" ":" |  sed -E 's/(chr[0-9XY]+:[0-9]+-[0-9]+),.+(chr[0-9XY]+:[0-9]+):.+/\1,\2/' | tr ":,-" "\t" | awk -v OFS="\t" '{print $1,$2,$3,$4,$5-2000,$5+2000}' | pgltools formatbedpe | sed 's/[ \t]*$//' | awk '{print $0"\tinteraction_"NR"\t.\t.\t."}'  > ${oprefix}.bedpem
```


#### merge 127 bedpe files:

see https://github.com/billgreenwald/pgltools/issues/3

```bash
find *csv | parallel -j 4 ./csv2bedpe.sh {}
mkdir bedpe
mv *bedpe bedpe/
cd bedpe

## takes very long time, let me try something else..., 3 million rows!
## take the unique rows! see here
## https://github.com/billgreenwald/pgltools/issues/3
## using pypy can speed up 
conda install -c ostrokach pypy


# the command line tool will figure out pypy is installed or not.
cat *bedpe | cut -f1-6 | sort | uniq | pgltools sort | pgltools merge -stdInA > ENCODE_merged_interaction.bedpe

```

#### split by chromosome to speed up 

```bash
cat *bedpe |  cut -f1-6 | sort | uniq |  pgltools sort > ENCODE_uniq_PE.bedpe

# split by chr
awk -F '\t' '{print >> $4".bedpe";close($4".bedpe")}' ENCODE_uniq_PE.bedpe

mkdir by_chr
mv chr*bedpe by_chr
cd by_chr
find *bedpe | parallel -j 6 'cat {} | pgltools sort | pgltools merge -stdInA > {/.}.merged.bedpe'

## final file for ENCODE
cat *merged.bedpe > ENCODE_EP.bedpe

```

add more columns for `rtrackylayer`:

```bash
cat encoderoadmap_lasso.*bedpe | awk '{print $0"\tinteraction_"NR"\t.\t.\t."}' > encoderoadmap_lasso_merged.bedpe

```

#### merge the regions using InteractionSet package

!!! warning
if merge all bedpe togeter, it is more than 3 million lines, and a naive merging will require a lot of RAM (more than 35G), the graph is using a lot 
of memories I think.

instead, do the recursive way below.

```{r}

library(rtracklayer)
library(InteractionSet)
library(here)

merge_bedpe<- function(gi){
  out<- findOverlaps(gi)
  library(RBGL)
  g <- ftM2graphNEL(as.matrix(out), W=NULL, V=NULL, edgemode="directed")
  edgemode(g) <- "undirected"
  connections <- connectedComp(g)
  cluster <- integer(length(gi))

  for (i in seq_along(connections)) {
    cluster[as.integer(connections[[i]])] <- i
    }
  boundingBox(gi, cluster)
}


list.files("data/enhancer_promoter_interaction/Encoderoadmap_EP_interaction/encoderoadmap_lasso/bedpe", pattern="bedpe")

bedpe_dir<- "data/enhancer_promoter_interaction/Encoderoadmap_EP_interaction/encoderoadmap_lasso/bedpe"

library(rtracklayer)

recursive_merge<- function(n){
  if (n == 1){
    bedpe<- import(here(bedpe_dir, "encoderoadmap_lasso.1.bedpe"), format = "bedpe")
    gi<- GInteractions(first(bedpe), second(bedpe), mode= "strict")
    merge_bedpe(gi)

  } else
  {
    bedpe_n<- import(here(bedpe_dir, paste0("encoderoadmap_lasso.", n, ".bedpe")), format = "bedpe")
    gi_n<- GInteractions(first(bedpe_n), second(bedpe_n), mode= "strict")
    gi_n_minus_1<- recursive_merge(n-1)
    merge_bedpe(c(gi_n, gi_n_minus_1))
  }
}

recursive_merge(1)
recursive_merge(2)

## total 127 files, it takes long, but will not crash your computer if you expect to see many overlapping pairs between samples.
# I submit the job to the shark cluster, and hopefully it finish in 72 hours!

merged_encode_bedpe<- recursive_merge(127)

```

### lasso prediction FANTOM5 data

```bash
mkdir FANTOM5_EP_interaction/
cd FANTOM5_EP_interaction/
wget http://yiplab.cse.cuhk.edu.hk/jeme/fantom5_lasso.zip

unzip fantom5_lasso.zip

cd fantom5_lasso

# much fewer rows than ENCODE data. CAGE enhancers are much fewer than H3K27ac defined.

find *csv | parallel -j 4 ./csv2bedpe.sh {}
mdkir bedpe
mv *bedpe bedpe/
cd bedpe

cat *bedpe |  cut -f1-6 | sort | uniq |  pgltools sort > FANTOM5_uniq_PE.bedpe

# split by chr
awk -F '\t' '{print >> $4".bedpe";close($4".bedpe")}' FANTOM5_uniq_PE.bedpe

mkdir by_chr
mv chr*bedpe by_chr
cd by_chr
find *bedpe | parallel -j 6 'cat {} | pgltools sort | pgltools merge -stdInA > {/.}.merged.bedpe'

## final file for FANTOM5
cat *merged.bedpe > FANTOM5_EP.bedpe

```

### combine the data sets

```bash
cd /home/mtang1/projects/CCLE_project/data/enhancer_promoter_interaction
mkdir ENCODE_FANTOM5_combined_EP_interaction
cp Encoderoadmap_EP_interaction/encoderoadmap_lasso/bedpe/by_chr/ENCODE_EP.bedpe ENCODE_FANTOM5_combined_EP_interaction/

cp FANTOM5_EP_interaction/fantom5_lasso/bedpe/by_chr/FANTOM5_EP.bedpe ENCODE_FANTOM5_combined_EP_interaction/

cd ENCODE_FANTOM5_combined_EP_interaction
 wc -l *
  594382 ENCODE_EP.bedpe
  143660 FANTOM5_EP.bedpe

mkdir merge_ENCODE_FANTOM5

 cp ../Encoderoadmap_EP_interaction/encoderoadmap_lasso/bedpe/ENCODE_uniq_PE.bedpe merge_ENCODE_FANTOM5/
 cp ../FANTOM5_EP_interaction/fantom5_lasso/bedpe/FANTOM5_uniq_PE.bedpe merge_ENCODE_FANTOM5/

cd merge_ENCODE_FANTOM5

cat *bedpe | sort | uniq |  pgltools sort > ENCODE_FANTOM5_uniq_PE.bedpe

# split by chr
awk -F '\t' '{print >> $4".bedpe";close($4".bedpe")}' ENCODE_FANTOM5_uniq_PE.bedpe

mkdir by_chr
mv chr*bedpe by_chr
cd by_chr
find *bedpe | parallel -j 6 'cat {} | pgltools sort | pgltools merge -stdInA > {/.}.merged.bedpe'

## final file for FANTOM5
cat *merged.bedpe > ENCODE_FANTOM5_EP.bedpe
```


### visualize bedpe format

`svtools bedpe2bed12` : this needs a more strict bedpe format, use `pgltools instead`:

`pgltools browser` 

```bash
pgltools browser -a ENCODE_EP.bedpe -tN ENCODE_EP > ENCODE_EP.bed
pgltools browser -a FANTOM5_EP.bedpe -tN FANTOM5_EP > FANTOM5_EP.bed
pgltools browser -a ENCODE_FANTOM5_EP.bedpe -tN ENCODE__FANTOM5_EP > ENCODE_FANTOM5_EP.bed

```

### link enhancer data to a refseq TSS

`pgltools` is very useful, but the algorithum requires bedpe file to be formated in a way that A is always comes before B:

```
#chrA startA  endA  chrB  startB  endB  
chr10 95800 97000 chr10 118103  122103
chr10 118103  122103  chr10 121800  123600
chr10 118103  122103  chr10 124600  127000
chr10 118103  122103  chr10 128600  128800
chr10 118103  122103  chr10 130200  130800
chr10 118103  122103  chr10 131000  132000
chr10 118103  122103  chr10 137200  139200
chr10 118103  122103  chr10 147000  147200
chr10 118103  122103  chr10 149800  150000

```

`chr10 95800 97000` should be before `chr10 118103  122103`.
we can not always keep enhancer in the first 3 columns, and promoters in the next 3 columns.

I will show you how to get around with it.

#### get the promoter region of all refseq genes

the refGene.txt was gotten by
`mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -D hg19 -e 'select * from refGene > refGene_hg19.txt`

or from the website

`wget  http://hgdownload.cse.ucsc.edu/goldenpath/hg19/database/refGene.txt.gz`

refGene.simple.txt was obtained by:     

`cut -f2,3,4,5,6,13 refGene.txt| awk '{print $2"\t"$4"\t"$5"\t"$1"\t"$6"\t"$3}' > refGene.simple.txt`

#### get tss for all refseq genes

```bash
# note it is 0 based bed file.
less -S refGene.simple.txt  | sed '1d' |  awk -v OFS="\t" '{if ($6=="+") print $1,$2,$2+1,$4,$5,$6; else print $1,$3-1,$3,$4,$5,$6}' > refseq_tss.bed

## 2kb flanking the tss sites. I could have used promoter() from GRanges to do the same, but let me stick to command line for now

# install all UCSC utilities https://github.com/ENCODE-DCC/kentUtils
fetchChromSizes hg19 > hg19_genome.info

bedtools slop -i refseq_tss.bed -g hg19_genome.info -b 2000 > refseq_promoter_2kb.bed

# sanity check, load the bed files to IGV and see!

```

#### post process the resulting file

if column 7 is A, column 4,5,6 is the location of the potential enhancer,
if column 7 is B, column 1,2,3 is the location of the potential enhancer. Reorder the columns. always put column 1,2,3 as the enhancer.

```bash
# sort first 
pgltools sort ENCODE_FANTOM5_EP.bedpe > ENCODE_FANTOM5_EP.sorted.bedpe

# this takes minutes
 pgltools intersect1D -a ENCODE_FANTOM5_EP.sorted.bedpe -b refseq_promoter_2kb.bed -wa -wb -allA | awk '$7=="A" || $7 == "B"' | awk -v OFS="\t" '{if ($7=="A") print $4,$5,$6,$1,$2,$3,"B",$8,$9,$10,$11,$12,$13; else print}' > ENCODE_FANTOM5_EP_refseq_promoter.tsv

```

Now, column 1-3 is always the potential enhancer, 4-6 is the region overlaps with the last three columns (refseq promoters).
Because the annotation used by lasso prediction maybe different from refseq, 
in fact, column 1-3 can be a promoter as well. mannually check in IGV the first
several lines and you will get what I am talking.

```bash
 head ENCODE_FANTOM5_EP_refseq_promoter.tsv
chr1  14800 15200 chr1  67091 71091 B NM_001005484  OR4F5 + chr1  67091 71091
chr1  67091 71091 chr1  16000 16400 B NR_106918 MIR6859-1 - chr1  15436 19436
chr1  67091 71091 chr1  16000 16400 B NR_107062 MIR6859-2 - chr1  15436 19436
chr1  16000 16400 chr1  67091 71091 B NM_001005484  OR4F5 + chr1  67091 71091
chr1  20200 20400 chr1  67091 71091 B NM_001005484  OR4F5 + chr1  67091 71091
chr1  79200 79800 chr1  67091 71091 B NM_001005484  OR4F5 + chr1  67091 71091
chr1  534000  534400  chr1  365640  369640  B NM_001005221  OR4F29  + chr1  365659  369659
chr1  534000  534400  chr1  365640  369640  B NM_001005224  OR4F3 + chr1  365659  369659
chr1  534000  534400  chr1  365640  369640  B NM_001005277  OR4F16  + chr1  365659  369659
chr1  534000  534400  chr1  620053  624053  B NM_001005221  OR4F29  - chr1  620034  624034

```